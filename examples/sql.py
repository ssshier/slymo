import re
from sly import Lexer

class SQLLexer(Lexer):
    tokens = { 'DQL', 'DML' }
    ignore = ' \t'
    reflags = re.I | re.S | re.M

    @_(r'SELECT.*?;')
    def DQL(self, t):
        return t

    @_(r'INSERT.*?;',r'UPDATE.*?;',r'DELETE.*?;')
    def DML(self, t):
        return t
    
    @_(r'\n+')
    def newline(self, t):
        self.lineno += t.value.count('\n')

    def error(self, t):
        print(f"Illegal charactor {t.value[0]}")
        self.index += 1


if __name__ == '__main__':
    statements = """
    SELECT * FROM TABLE;
    INSERT INTO TABLE;
    UPDATE TABLE SET;
    DELETE TABLE;
    """
    lexer = SQLLexer()
    resutls = lexer.tokenize(statements)
    for result in resutls:
        print(result)
